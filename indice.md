# Documentación

1. [Metodología de desarrollo](docs/metodologia.md "alt")

2. [Modelo de branching](docs/branching.md "alt")

3. [Toma de requerimientos](docs/requirements.md "alt")

4. [Historias de usuario](docs/userhistories.md "alt")

5. [Tecnologías a usar](docs/tecs.md "alt")

6. [Diagramas de actividades](docs/activities.md "alt")

7. [Listado y descripción de microservicios](docs/microservices.md "alt")

8. [Contratos de servicios](docs/contracts.md "alt")

9. [Arquitectura del sistema](docs/arquitectura.md "alt")

10. [Diagrama de datos](docs/datos.md "alt")

11. [Seguridad de la aplicación](docs/seguridad.md "alt")

12. [Definición de indicadores claves para el negocio](docs/kpi.md "alt")

13. [Análisis de UX y UI ](docs/ux.md "alt")

14. [Mockups](docs/mockups.md "alt")

15. [Pipelines](docs/pipelines.md "alt")
