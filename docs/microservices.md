## Microservicios

| Microservicio                 | Descripción                                                                            |
| ----------------------------- | -------------------------------------------------------------------------------------- |
| registro                      | Crea un nuevo usuario (turista,admin)                                                  |
| registro de terceros          | Crea un nuevo usuario que ofrece servicios de terceros (Hotel, Renta-autos) en la DB   |
| login                         | Autoriza o deniega el ingreso de un usuario al sistema                                 |
| Crear Espacio                 | Crea un espacio para ser reservado                                                     |
| Listar espacios               | Retorna todos los espacios dadas unas condiciones                                      |
| Hacer reservación             | Crea una reservación a nombre de un usuario turista                                    |
| Ingresar vehículo             | Crea un nuevo vehículo asociado a un Renta-autos                                       |
| Cargar vehículos              | Crea n cantidad de autos contenidos en un archivo csv                                  |
| Listar vacunados              | Retorna la lista de los turistas y su esquema de vacunación                            |
| Generar certificado           | Retorna una imagen codificada en base64 con el certificado de vacunación de un turista |
| Definir esquema de vacunación | Crea un nuevo esquema de vacunación                                                    |
| Editar esquema de vacunación  | Actualiza un esquema de vacunación existente                                           |
| Crear vuelo                   | Crea un vuelo en la DB                                                                 |
| Validar tarjeta de crédito    | Autoriza o deniega el uso de una tarjeta de crédito                                    |
| Hacer reseña                  | Crea un nueva reseña de un usuario sobre un servicio                                   |
| Ver reseñas                   | Retorna la lista de todas las reseñas                                                  |
| Ver reseña                    | Retorna una reseña en específico                                                       |
| Ver resultados                | Retorna los reportes del sistema                                                       |

[Volver al índice](../indice.md "Título opcional del enlace")
