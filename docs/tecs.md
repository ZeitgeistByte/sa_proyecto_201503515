| Tecnología   | Descripción                                                                                                                                                                       |
| ------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Angular      | Framework que se utilizará en el frontend para crear la aplicación web en formato singlepage, por consecuencia se utilizarán Typescript y Javascript como lenguajes para frontend |
| Mysql        | Base de datos tipo relacional que se utilizará para las operaciones transaccionales del sistema                                                                                   |
| Flask        | Framework escrito en Python que se utilizará para crear los servicios REST y SOAP de la aplicación                                                                                |
| Capacitor    | Software open source para la creación de aplicaciones móviles nativas                                                                                                             |
| Bulma        | Software open source, framework css que se puede utilizar para construir interfaces web responsivasd                                                                              |
| Docker       | Será utilizado para desplegar los microservicios y darle independencia a cada los mismos                                                                                          |
| Terraform    | Plataforma para la automatización de infraestructura (IaC)                                                                                                                        |
| Amazon EC2   | Máquinas virtuales (Servidores) Para frontend, bases de datos relacionales y contenedores                                                                                         |
| Gitlab CI/CD | Software de desarrollo para la realización e implementación del ciclo DevOps                                                                                                      |

[Volver al índice](../indice.md "Título opcional del enlace")
