# KPI

| KPI                                | Descripción                                                                  |
| ---------------------------------- | ---------------------------------------------------------------------------- |
| Cantidad de usuarios               | Cantidad de usuarios de cada tipo registrados en el sistema                  |
| Cantidad de usuarios activos       | Usuarios que han consumido al menos un servicio en los últimos 30 días       |
| Ingresos generados por hoteles     | Dinero generado por las reservaciones de hotel en los úlimos 30 días         |
| Ingresos generados por renta autos | Dinero generado por las reservaciones de autos en los úlimos 30 días         |
| Ingresos generados por vuelos      | Dinero generado por las reservas de vuelos en los úlimos 30 días             |
| Tasa de satisfacción por servicios | Tasa de satisfacción tomada de acuerdo con las reseñas en los úlimos 30 días |
| Ingresos generados por los bancos  | Dinero generado por comisión para los bancos en los úlimos 30 días           |

[Volver al índice](../indice.md "Título opcional del enlace")
