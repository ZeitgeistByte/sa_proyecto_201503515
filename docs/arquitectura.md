# Arquitectura de la solución

SOA es una arquitectura que se utiliza específicamente para aplicaciones empresariales, también funciona como una caja negra los componentes de esta arquitectura, no son vistos o percibidos por el usuario, sino que se esconde completamente de ellos. En el diagrama la comunicación entre el ESB y los microservicios se hace con una flecha morada, pero es necesario aclarar que el ESB se comunica con cada uno de los microservicios, se represento así por cuestiones de estética.

Se utilizará una base de datos no relacional para los logs del ESB y un clúster de bases de datos para una alta disponibilidad.

![Casos de uso](diagrams/arquitecura.png "Arquitectura")

[Volver al índice](../indice.md "Título opcional del enlace")
