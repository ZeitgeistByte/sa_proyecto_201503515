## Justificación de la app web y móvil.

Hoy en día, 4 de cada 5 personas cuentan con un smartphone o dispositivo electrónico que les permite establecer conexión para acceder a internet. El desarrollo de aplicaciones móviles y de páginas webs se ha convertido en los últimos años en un elemento esencial para cualquier compañía. Además que estas aplicaciones nos permitirán centralizar servicios indispensables para turistas de cualquier parte del mundo.

La creación de este nuevo medio puede suponer poseer una plataforma exclusiva cargada de funcionalidades únicas para nuestros clientes turistas consiguiendo que el usuario se sienta valorado al recibir un trato especial.

## Características UX

1. Deseable: Se utilizará un logotipo reconocible de la empresa y la paleta de colores del sitio web y de la app móvil giraran en torno a este logo. Para dar una sensación de armonía.
2. Accesibilidad: Se utilizará una fuente de tamaño cómodo incluso para personas con problemas visuales, haciendo así que la aplicación web y móvil lleguen a un mayor número de personas sin exclusión.
3. Confiabilidad: Se mantendrán actualizados los enlaces del sitio web y se garantizará que los datos ingresados por el usuario que puedan ser sensibles, como los datos de sus tarjetas de crédito, no serán almacenados sin cifrar.
4. Navegabilidad: Se mantendrá siempre a la vista el menú con las opciones a las que el usuario puede acceder, haciendo más fácil que el usuario pueda ir más fácil de un lado a otro dentro del sitio y la app móvil.
5. Utilidad: Se mostrará al usuario únicamente las opciones que le interesan, los procesos serán sencillos y sin rodeos, pudiendo hacer directamente lo que desee.

## Características UI

1. Para la UI se utilizan principalmente formularios, para registros y creación de nuevos elementos.
2. Se procura la reutilización de plantillas, esto para garantizar la uniformidad visual de los procesos.
3. Para las búsquedas se presenta la posibilidad de realizar filtraciones para mostrar a los usuarios únicamente los elementos que coincidan con sus intereses.

[Volver al índice](../indice.md "Título opcional del enlace")
