# Full Trip

## Metodología de desarrollo

Existen diversas metodologías de desarrollo de software, tanto tradicionales como ágiles. Para este proyecto definitivamente una metodología ágil es la mejor opción, ya que se realizaran integraciones y despliegues continuos, ahora bien, al existir la incertidumbre sobre la cantidad de personas que desarrollarán el proyecto, se puede descartar la metodología SCRUM ya que en ella se ven involucrados varios roles, pero, si bien una de las características de SCRUM, donde se divide el trabajo del proyecto en tareas o "tarjetas" de tareas, se puede optar por una metodología que si bien no es iterativa es incremental y que por lo tanto es ágil también, como lo es <br> Kanban </br> en esta metodología no existen los roles como tal entonces si en caso se llegaran a integrar nuevos miembros al equipo se dividirían las tarjetas aún por resolver entre los nuevos integrantes, dando lugar a un trabajo más ordenado.

Además se pueden clasificar estas tarjetas por ejemplo en un tamaño (grande, mediano, pequeño), cada miembro tomará ciertas tareas y serán partes de su WIP (Work in progress), estableciendo un límite de tareas para garantizar que se terminen de desarrollar todas de la manera correcta.

[Volver al índice](../indice.md "Título opcional del enlace")
