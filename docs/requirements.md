# Full Trip

## Toma de requerimientos

### Misión

Garantizar una completa y agradable experiencia en época post pandemia para los turistas que desean realizar viajes y tomar vuelos internacionales hacia o desde nuestro país, además de cumplir con los estándares de salud al permitir únicamente viajes de personas que cumplan con esquemas de vacunación completos.

### Stakeholders del proyecto

En este proyecto se ven varias organizaciones involucradas directa o indirectamente, podemos definirlos de la siguiente manera:
| Nombre | Descripción |
|--------------|------|
| Inguat| Instituo Guatemalteco de turismo, interesado porque se cumplan los 4 ejes complementarios que tienen establecidos, especialmente por la situación actual las condiciones de seguridad|
| Empresa turística| Empresa que necesita que la aplicación sea desarrollada, serán administradores del sistema |
| Turista| Persona que desea realizar un viaje a cualquier parte del mundo|
| Hoteles | Entidades que desean asegurar ganancias al anunciar sus servicios en el sistema |
|Empresas de renta de autos| Empresas que ponen a disposición de los turistas automóviles para ser alquilados|
|Otros comercios formales o informales| Al tener mayor cantidad de turistas incrementan las posibilidades de obtener un mayor número de ventas |

### Casos de Uso

![Casos de uso](diagrams/cdu.png "Casos de uso")

### Requerimientos funcionales

| Funcion     | Registrar turista                                                         |
| ----------- | ------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir registrar un nuevo usuario turista en el sistema |
| Entradas    | Nombre, País, Correo, Contraseña                                          |
| Salidas     | Fallo o éxito de la creación del perfil                                   |

| Funcion     | Registrar servicio de tercero                                                                  |
| ----------- | ---------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir el registro de un nuevo servicio de tercero (Hotel o reserva de auto) |
| Entradas    | Nombre de la empresa, tipo de servicio, dirección, teléfono, correo, contraseña                |
| Salidas     | Fallo o éxito en la creación del usuario                                                       |

| Funcion     | Inicio de sesión                                                             |
| ----------- | ---------------------------------------------------------------------------- |
| Descripcion | El usuario con una cuenta registrada debe poder iniciar sesión en el sistema |
| Entradas    | correo, contraseña                                                           |
| Salidas     | Acceso o denegación del acceso al sistema de acuerdo a la entrada            |

| Funcion     | Recuperar contraseña                                                 |
| ----------- | -------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir a cualquier usuario recuperar su contraseña |
| Entradas    | correo                                                               |
| Salidas     | Mensaje con instrucciones al usuario                                 |

| Funcion     | Crear cuenta de administrador                                                                     |
| ----------- | ------------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir a los usuarios de tipo administrador crear otros usuarios del mismo tipo |
| Entradas    | Correo, contraseña                                                                                |
| Salidas     | Fallo o éxito en la creación del usuario                                                          |

| Funcion     | Crear espacio para reserva                                                                                   |
| ----------- | ------------------------------------------------------------------------------------------------------------ |
| Descripcion | Un usuario de tipo hotel debe poder poner a disposición de los usuarios turistas una habitación para reserva |
| Entradas    | País, ciudad, cantidad de personas, rango de precio y fechas de disponibilidad de la reserva                 |
| Salidas     | Fallo o éxito de la operación                                                                                |

| Funcion     | Ver espacios reservados                                                                                    |
| ----------- | ---------------------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir a los usuarios tipo Hotel ver todos los espacios y el estado en que se encuentran |
| Entradas    | Ninguna                                                                                                    |
| Salidas     | Lista de espacios                                                                                          |

| Funcion     | Hacer reservación                                                                                     |
| ----------- | ----------------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir a los usuarios tipo Turista reservar los espacios publicados por los Hoteles |
| Entradas    | Espacio que desea reservar                                                                            |
| Salidas     | Fallo o éxito en la reserva                                                                           |

| Funcion     | Buscar espacio                                                                                                         |
| ----------- | ---------------------------------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir a los usuarios tipo turista buscar y aplicar filtros a sus búsquedas de espacios para reserva |
| Entradas    | País, ciudad, cantidad de personas, rango de precio y fechas de disponibilidad de la reserva                           |
| Salidas     | Elementos que cumplen con los filtros de búsqueda                                                                      |

| Funcion     | Ingresar vehículo                                                                                            |
| ----------- | ------------------------------------------------------------------------------------------------------------ |
| Descripcion | El sistema debe permitir al usuario tipo Renta-autos publicar un vehículo y ponerlo disponible para alquiler |
| Entradas    | Tipo de vehículo, marca, modelo, número de asientos, precio                                                  |
| Salidas     | Fallo o éxito de creación de vehículo                                                                        |

| Funcion     | Cargar vehículos                                                                                                                        |
| ----------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir al usuario tipo Renta-autos subir un archivo que contenga 1 o más vehículos para ser ingresados más facilmente |
| Entradas    | Archivo con extensión csv                                                                                                               |
| Salidas     | Fallo o éxito de creación de vehículos                                                                                                  |

| Funcion     | Listar vacunados                                                                                                                                 |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| Descripcion | El sistema debe poder llevar un registro de todas las personas que han sido vacunadas, y que vacuna han obtenido y si tienen el esquema completo |
| Entradas    | Ninguna                                                                                                                                          |
| Salidas     | Listado de usuarios tipo turista con los detalles sobre su esquema de vacunación                                                                 |

| Funcion     | Descargar certificado                                                                                                                                                      |
| ----------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir a los usuarios tipo turistas con un esquema de vacunación completo descargar un certificado digital con código QR que los acredite como vacunados |
| Entradas    | Ninguna                                                                                                                                                                    |
| Salidas     | Archivo con el código QR                                                                                                                                                   |

| Funcion     | Definir esquema de vacunación                                                                 |
| ----------- | --------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir a los usuarios administradores definir nuevos esquemas de vacunación |
| Entradas    | Nombre de vacuna, número de dosis                                                             |
| Salidas     | Fallo o éxito en la creación del esquema                                                      |

| Funcion     | Redefinir esquema de vacunación                                                                                                                          |
| ----------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe permitir a los usuarios administrador editar un esquema existente y ampliar el número de dósis para que se considere un esquema completo |
| Entradas    | Nombre de vacuna, nuevo número de dosis                                                                                                                  |
| Salidas     | Fallo o éxito en la redefinición del esquema                                                                                                             |

| Funcion     | Crear vuelo                                                                                   |
| ----------- | --------------------------------------------------------------------------------------------- |
| Descripcion | El usuario tipo adminsitrador debe poder crear vuelos de forma manual                         |
| Entradas    | Ciudad origen, ciudad destino, horario, número de pasajeros, tipo de viaje (ida o ida-vuelta) |
| Salidas     | Fallo o éxito en la creación de vuelo                                                         |

| Funcion     | Crear vuelos                                                          |
| ----------- | --------------------------------------------------------------------- |
| Descripcion | El usuario tipo adminsitrador debe poder crear vuelos de forma masiva |
| Entradas    | Archivo csv con los datos de 1 o más vuelos                           |
| Salidas     | Fallo o éxito en la creación de los vuelos                            |

| Funcion     | Validación de tarjeta de crédito                                                                                              |
| ----------- | ----------------------------------------------------------------------------------------------------------------------------- |
| Descripcion | El sistema debe ser capaz de contrastar los datos de la tarjeta de un usuario para saber que se trata de una tarjeta legítima |
| Entradas    | Nombre, fecha de vencimiento, CVV, número de tarjeta                                                                          |
| Salidas     | Fallo o éxito en la validación                                                                                                |

| Funcion     | Hacer una reseña                                                                                      |
| ----------- | ----------------------------------------------------------------------------------------------------- |
| Descripcion | El usuario tipo Turista debe poder escribir una reseña sobre los servicios que ha adquirido/consumido |
| Entradas    | Servicio, reseña                                                                                      |
| Salidas     | Mensaje de creación de la reseña                                                                      |

| Funcion     | Ver las reseñas                                                                                                                       |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| Descripcion | Los usuarios tipo turista y administrador deben ser capaces de ver las reseñas escritas en el sistema sobre los servicios de terceros |
| Entradas    | Servicio                                                                                                                              |
| Salidas     | Lista de reseñas                                                                                                                      |

| Funcion     | Ver resultados                                                                                                  |
| ----------- | --------------------------------------------------------------------------------------------------------------- |
| Descripcion | El usuario tipo administrador debe ser capaz de ver resultados o reportes sobre los datos que genera el sistema |
| Entradas    | Selección de inidicador                                                                                         |
| Salidas     | Reporte                                                                                                         |

### Requerimientos no funcionales

| Requerimiento  | Descripción                                                                                                                                                                                                         |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Rendimiento    | Las páginas del sitio web en cualquier navegador deben tomar menos de 6 segundos de respuesta                                                                                                                       |
| Compatibilidad | El sistema debe de ser mostrado y funcionar correctamente en todos los navegadores modernos actuales y en dispositivos Android 9 o superior                                                                         |
| Disponibilidad | El dashboard de cada usuario debe estar disponible el 98% por ciento de las veces que se intente acceder, sin importar la hora                                                                                      |
| Seguridad      | No se deben de almacenar en ningún momento los datos de la tarjeta de crédito de los usuarios.<br>Se debe utilizar una verificación por medio de tokens para garantizar la seguridad de las cuentas de los usuarios |
| Usabilidad     | La tasa de errores por parte del usuario para ingresar sus datos de pago no deben de exceder el 10%                                                                                                                 |
| Reusabilidad   | No existen módulos duplicados                                                                                                                                                                                       |
| Robustez       | Únicamente un máximo de 1% de los eventos causan un error en el sistema                                                                                                                                             |

<br>[Volver al índice](../indice.md "Título opcional del enlace")
