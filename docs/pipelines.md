# Pipelines

## Trigger

En la etapa inicial, el pipeline de CI/CD se activa mediante un commit en el repositorio de código. Cualquier cambio en la rama develop lanzará la iniciación del pipeline.

## Build

Esta es la segunda etapa de CI/CD en la que fusiona el código fuente y sus dependencias. Se realiza principalmente para crear una instancia ejecutable de todos los microservicios. Además se empaquetan en contenedores de docker.

## Test

La etapa de prueba incluye la ejecución de pruebas automatizadas para validar la corrección del código y el comportamiento del software. Esta etapa evita que los errores fácilmente reproducibles lleguen a los clientes. Se harán pruebas de frontend y de los microservicios.

## Deploy

Esta es la última etapa en la que su producto se pone en marcha. Una vez que la compilación ha superado con éxito todos los escenarios de prueba requeridos, está lista para implementarse en el servidor en vivo.

[Volver al índice](../indice.md "Título opcional del enlace")
