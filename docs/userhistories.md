| Historia de usuario | 1                                                      |
| ------------------- | ------------------------------------------------------ |
| Como usuario        | administrador                                          |
| Quiero              | ver resultados de reportes                             |
| Para                | conocer las estadísticas y principales kpi del sistema |

| Historia de usuario | 2                                                                                     |
| ------------------- | ------------------------------------------------------------------------------------- |
| Como usuario        | turista                                                                               |
| Quiero              | ver las reseñas sobre los servicios                                                   |
| Para                | tener más datos y tomar mejores decisiones con base en experiencias de otros usuarios |

| Historia de usuario | 3                                                                         |
| ------------------- | ------------------------------------------------------------------------- |
| Como usuario        | administrador                                                             |
| Quiero              | ver las reseñas sobre los servicios                                       |
| Para                | tener una idea general del nivel de satisfacción de los usuarios turistas |

| Historia de usuario | 4                                                               |
| ------------------- | --------------------------------------------------------------- |
| Como usuario        | turista                                                         |
| Quiero              | poder escribir reseñas                                          |
| Para                | compartir mi experiencia con ciertos servicios a otros usuarios |

| Historia de usuario | 5                                            |
| ------------------- | -------------------------------------------- |
| Como usuario        | Administrador                                |
| Quiero              | Crear vuelos de forma masiva                 |
| Para                | Ahorrar tiempo y no crearlos individualmente |

| Historia de usuario | 6                                              |
| ------------------- | ---------------------------------------------- |
| Como usuario        | Administrador                                  |
| Quiero              | Crear un vuelo de forma manual                 |
| Para                | Ponerlo a disposición de los usuarios turistas |

| Historia de usuario | 7                                                                           |
| ------------------- | --------------------------------------------------------------------------- |
| Como usuario        | administrador                                                               |
| Quiero              | definir un esquema de vacunación                                            |
| Para                | que los usuarios turistas puedan indicar que tipo de vacuna se han aplicado |

| Historia de usuario | 8                                                                           |
| ------------------- | --------------------------------------------------------------------------- |
| Como usuario        | administrador                                                               |
| Quiero              | redefinir un esquema de vacunación                                          |
| Para                | adecuar los esquemas a la situación actual que establecen las farmacéuticas |

| Historia de usuario | 9                                                                         |
| ------------------- | ------------------------------------------------------------------------- |
| Como usuario        | Turista                                                                   |
| Quiero              | Descargar un certificado                                                  |
| Para                | comprobar ante otras personas que poseo un esquema de vacunación completo |

| Historia de usuario | 10                                                              |
| ------------------- | --------------------------------------------------------------- |
| Como usuario        | administrador                                                   |
| Quiero              | listar los vacunados                                            |
| Para                | ver que tipo de vacuna se han aplicado los turistas del sistema |

| Historia de usuario | 11                                                                       |
| ------------------- | ------------------------------------------------------------------------ |
| Como usuario        | renta-autos                                                              |
| Quiero              | insertar un vehículo                                                     |
| Para                | ofrecerlo como disponible para ser rentado por cualquier usuario turista |

| Historia de usuario | 12                                                  |
| ------------------- | --------------------------------------------------- |
| Como usuario        | renta-autos                                         |
| Quiero              | cargar vehículos de manera masiva                   |
| Para                | reducir el tiempo empleado si los cargara uno a uno |

| Historia de usuario | 13                                              |
| ------------------- | ----------------------------------------------- |
| Como usuario        | turista                                         |
| Quiero              | poder aplicar filtros a mis búsquedas           |
| Para                | especificar mejor el espacio que deseo reservar |

| Historia de usuario | 14                                        |
| ------------------- | ----------------------------------------- |
| Como usuario        | turista                                   |
| Quiero              | seleccionar un espacio                    |
| Para                | reservarlo eligiendo una fecha específica |

| Historia de usuario | 15                                                                         |
| ------------------- | -------------------------------------------------------------------------- |
| Como usuario        | hotel                                                                      |
| Quiero              | ver los espacios que tengo creados                                         |
| Para                | ver el estado en que se encuentran y fechas en las que han sido reservados |

| Historia de usuario | 16                                           |
| ------------------- | -------------------------------------------- |
| Como usuario        | hotel                                        |
| Quiero              | poder crear un espacio para reserva          |
| Para                | que los usuarios turistas puedan reservarlos |

| Historia de usuario | 17                                                                                                  |
| ------------------- | --------------------------------------------------------------------------------------------------- |
| Como usuario        | adminstrador                                                                                        |
| Quiero              | crear otros usuarios administradores                                                                |
| Para                | que más personas tengan acceso a las funcionalidades de administrador y distribuir mejor el trabajo |

| Historia de usuario | 18                                                 |
| ------------------- | -------------------------------------------------- |
| Como usuario        | de cualquier tipo                                  |
| Quiero              | poder recuperar mi contraseña                      |
| Para                | no perder acceso a mi cuenta en caso que la olvide |

| Historia de usuario | 19                    |
| ------------------- | --------------------- |
| Como usuario        | de cualquier tipo     |
| Quiero              | poder logearme        |
| Para                | hacer uso del sistema |

| Historia de usuario | 20                           |
| ------------------- | ---------------------------- |
| Como usuario        | turista, servicio de tercero |
| Quiero              | poder crear una cuenta       |
| Para                | hacer uso del sistema        |

[Volver al índice](../indice.md "Título opcional del enlace")
