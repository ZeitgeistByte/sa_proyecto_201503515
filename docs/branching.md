# FULL TRIP
## Metodología de branching

Para seleccionar la técnica de brancheo a utilizar se estudiaron los pros y contras que las diferentes alternativas ofrecen, tomando en cuenta que estas técnicas son un estándar que dicta como el equipo de desarrollo usa las ramas dentro del repositorio del proyecto.

Las técnicas de brancheo analizadas son la estrategia de brancheo por features en la cual se crean ramas para características o tareas específicas del software y la estrategia de brancheo por releases en la cual se crea una rama para una potencial release.

Se definen los pros y contras en la siguiente tabla de decisión.

| Estrategia   | Pros | Contras |
|--------------|------|---------|
| Por features |  Permite a los desarrolladores trabajar en una característica sin afectar el core del proyecto <br> Da al equipo la libertad de innovar <br>Permite implementar CI/CD    | Las ramas de feature se pueden extender mucho. <br> Solo funciona si los desarrolladores hacen continuas integraciones     |
| Por releases |Útil en proyectos que soportan múltiples versiones en paralelo      |      Se crea más trabajo para los desarrolladores, al tener que mantener varias versiones al mismo tiempo.   |

Tomando en cuenta las caracteristicas del proyecto, al utilizar CI/CD es mejor utilizar una técnica de brancheo que sea compatible con esto, por lo cual se escoge una estrategia de brancheo por features como <strong>Gitflow</strong>.

[Volver al índice](../indice.md "Título opcional del enlace")