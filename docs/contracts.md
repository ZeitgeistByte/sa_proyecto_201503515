## Servicio: Registro

|                    |                                       |
| ------------------ | ------------------------------------- |
| Descripción        | Crea un nuevo usuario (turista,admin) |
| Ruta               | /user                                 |
| Método             | POST                                  |
| Formato de entrada | JSON                                  |

### Parámetros de entrada

| Atributo   | Tipo   | Descripción                         |
| ---------- | ------ | ----------------------------------- |
| Correo     | cadena | Correo único en el sistema          |
| Contraseña | cadena | Contraseña para ingresar            |
| Tipo       | entero | 0 para turista 1 para administrador |
| Nombre     | cadena | Nombre y apellido del usuario       |
| País       | entero | País de origen del usuario          |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro del usuario       |
| message  | cadena  | Mensaje de resultado de la operación |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro del usuario       |
| message  | cadena  | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio: registro de terceros

|                    |                                                                                      |
| ------------------ | ------------------------------------------------------------------------------------ |
| Descripción        | Crea un nuevo usuario que ofrece servicios de terceros (Hotel, Renta-autos) en la DB |
| Ruta               | /thirdparty                                                                          |
| Método             | POST                                                                                 |
| Formato de entrada | JSON                                                                                 |

### Parámetros de entrada

| Atributo             | Tipo   | Descripción                   |
| -------------------- | ------ | ----------------------------- |
| Correo               | cadena | correo de la empresa          |
| Contraseña           | cadena | contraseña de la empresa      |
| Nombre de la empresa | cadena | Nombre completo de la empresa |
| Tipo de servicio     | entero | 0 hotal - 1 renta-autos       |
| Dirección            | cadena | Dirección de la empresa       |
| Teléfono             | cadena | Teléfono de la empresa        |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro de la empresa     |
| message  | cadena  | Mensaje de resultado de la operación |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro de la empresa     |
| message  | cadena  | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio: Login

|                    |                                       |
| ------------------ | ------------------------------------- |
| Descripción        | Crea un nuevo usuario (turista,admin) |
| Ruta               | /login                                |
| Método             | POST                                  |
| Formato de entrada | JSON                                  |

### Parámetros de entrada

| Atributo   | Tipo   | Descripción                       |
| ---------- | ------ | --------------------------------- |
| correo     | cadena | correo del usuario                |
| contraseña | cadena | contraseña codificada del usuario |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción        |
| -------- | ------- | ------------------ |
| access   | boolean | Login exitoso o no |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo   | Descripción           |
| -------- | ------ | --------------------- |
| error    | cadena | Descripción del error |

<hr style="border:2px solid gray"> </hr>

## Servicio: Crear espacio

|                    |                                    |
| ------------------ | ---------------------------------- |
| Descripción        | Crea un espacio para ser reservado |
| Ruta               | /space                             |
| Método             | POST                               |
| Formato de entrada | JSON                               |

### Parámetros de entrada

| Atributo                | Tipo   | Descripción                                    |
| ----------------------- | ------ | ---------------------------------------------- |
| id_hotel                | entero | identificador del hotal que crea el espacio    |
| país                    | entero | Código del país dónde se ubica                 |
| num_personas            | entero | Número de personas máximo para usar el espacio |
| precio                  | entero | precio del espacio                             |
| fecha de disponibilidad | fecha  | fecha en que está disponible el espacio        |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo | Descripción       |
| -------- | ---- | ----------------- |
| result   | int  | 0 fallo - 1 éxito |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

<hr style="border:2px solid gray"> </hr>

## Servicio: Listar espacios

|                    |                                                   |
| ------------------ | ------------------------------------------------- |
| Descripción        | Retorna todos los espacios dadas unas condiciones |
| Ruta               | /space                                            |
| Método             | GET                                               |
| Formato de entrada | JSON                                              |

### Parámetros de entrada

| Atributo                | Tipo   | Descripción                                    |
| ----------------------- | ------ | ---------------------------------------------- |
| país                    | entero | Código del país dónde se ubica                 |
| num_personas            | entero | Número de personas máximo para usar el espacio |
| precio                  | entero | precio del espacio                             |
| fecha de disponibilidad | fecha  | fecha en que está disponible el espacio        |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo  | Descripción                                          |
| -------- | ----- | ---------------------------------------------------- |
| data     | array | Arreglo con los espacios que cumplen las condiciones |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo  | Descripción   |
| -------- | ----- | ------------- |
| data     | array | arreglo vacío |

<hr style="border:2px solid gray"> </hr>

## Servicio: Hacer reservación

|                    |                                                     |
| ------------------ | --------------------------------------------------- |
| Descripción        | Crea una reservación a nombre de un usuario turista |
| Ruta               | /reservation                                        |
| Método             | POST                                                |
| Formato de entrada | JSON                                                |

### Parámetros de entrada

| Atributo   | Tipo   | Descripción                  |
| ---------- | ------ | ---------------------------- |
| id_espacio | entero | id del espacio que reservará |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción            |
| -------- | ------- | ---------------------- |
| result   | boolean | Operación exitosa o no |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo      | Tipo   | Descripción               |
| ------------- | ------ | ------------------------- |
| error_message | cadena | Muestra que error sucedio |

<hr style="border:2px solid gray"> </hr>

## Servicio: Ingresar vehículo

|                    |                                                  |
| ------------------ | ------------------------------------------------ |
| Descripción        | Crea un nuevo vehículo asociado a un Renta-autos |
| Ruta               | /car                                             |
| Método             | POST                                             |
| Formato de entrada | JSON                                             |

### Parámetros de entrada

| Atributo | Tipo   | Descripción                        |
| -------- | ------ | ---------------------------------- |
| tipo     | entero | Identificador del tipo de vehículo |
| marca    | entero | identificador de marca             |
| modelo   | entero | modelo                             |
| asientos | entero | número de asientos                 |
| precio   | doble  | precio de alquiler en dolares      |
| estado   | entero | 0 no disponible - 1 disponible     |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción |
| -------- | ------- | ----------- |
| result   | boolean |             |
| data     | JSON    |             |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción |
| -------- | ------- | ----------- |
| result   | boolean |             |

<hr style="border:2px solid gray"> </hr>

## Servicio: Cargar vehículos

|                    |                                                       |
| ------------------ | ----------------------------------------------------- |
| Descripción        | Crea n cantidad de autos contenidos en un archivo csv |
| Ruta               | /cars                                                 |
| Método             | POST                                                  |
| Formato de entrada | JSON                                                  |

### Parámetros de entrada

| Atributo | Tipo   | Descripción                  |
| -------- | ------ | ---------------------------- |
| csv      | cadena | archivo codificado en base64 |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción                                                       |
| -------- | ------- | ----------------------------------------------------------------- |
| result   | boolean |                                                                   |
| failed   | array   | Arreglo con el número de líneas de los elementos que tenían error |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

<hr style="border:2px solid gray"> </hr>

## Servicio: Listar vacunados

|                    |                                                             |
| ------------------ | ----------------------------------------------------------- |
| Descripción        | Retorna la lista de los turistas y su esquema de vacunación |
| Ruta               | /vaccined                                                   |
| Método             | GET                                                         |
| Formato de entrada | JSON                                                        |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo  | Descripción                            |
| -------- | ----- | -------------------------------------- |
| data     | ARRAY | arreglo con los datos de los vacunados |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

<hr style="border:2px solid gray"> </hr>

## Servicio:

|                    |      |
| ------------------ | ---- |
| Descripción        |      |
| Ruta               | /    |
| Método             |      |
| Formato de entrada | JSON |

### Parámetros de entrada

| Atributo | Tipo | Descripción |
| -------- | ---- | ----------- |
|          |      |             |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción                      |
| -------- | ------- | -------------------------------- |
| result   | boolean | Estado de registro de la empresa |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro de la empresa     |
| message  | cadena  | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio:

|                    |      |
| ------------------ | ---- |
| Descripción        |      |
| Ruta               | /    |
| Método             |      |
| Formato de entrada | JSON |

### Parámetros de entrada

| Atributo | Tipo | Descripción |
| -------- | ---- | ----------- |
|          |      |             |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción                      |
| -------- | ------- | -------------------------------- |
| result   | boolean | Estado de registro de la empresa |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro de la empresa     |
| message  | cadena  | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio:

|                    |      |
| ------------------ | ---- |
| Descripción        |      |
| Ruta               | /    |
| Método             |      |
| Formato de entrada | JSON |

### Parámetros de entrada

| Atributo | Tipo | Descripción |
| -------- | ---- | ----------- |
|          |      |             |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción                      |
| -------- | ------- | -------------------------------- |
| result   | boolean | Estado de registro de la empresa |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro de la empresa     |
| message  | cadena  | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio:

|                    |      |
| ------------------ | ---- |
| Descripción        |      |
| Ruta               | /    |
| Método             |      |
| Formato de entrada | JSON |

### Parámetros de entrada

| Atributo | Tipo | Descripción |
| -------- | ---- | ----------- |
|          |      |             |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción                      |
| -------- | ------- | -------------------------------- |
| result   | boolean | Estado de registro de la empresa |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro de la empresa     |
| message  | cadena  | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio: Generar certificado

|                    |                                                                                        |
| ------------------ | -------------------------------------------------------------------------------------- |
| Descripción        | Retorna una imagen codificada en base64 con el certificado de vacunación de un turista |
| Ruta               | /certificate/id                                                                        |
| Método             | GET                                                                                    |
| Formato de entrada | PARAMETER                                                                              |

### Parámetros de entrada

| Atributo | Tipo   | Descripción    |
| -------- | ------ | -------------- |
| id       | string | id del turista |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo   | Descripción    |
| -------- | ------ | -------------- |
| image    | cadena | Base 64 del qr |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo   | Descripción                          |
| -------- | ------ | ------------------------------------ |
| message  | cadena | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio: Definir esquema de vacunación

|                    |                                     |
| ------------------ | ----------------------------------- |
| Descripción        | Crea un nuevo esquema de vacunación |
| Ruta               | /scheme                             |
| Método             | POST                                |
| Formato de entrada | JSON                                |

### Parámetros de entrada

| Atributo | Tipo   | Descripción                 |
| -------- | ------ | --------------------------- |
| nombre   | cadena | Nombre de la vacuna         |
| dosis    | entero | Número de dosis del esquema |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción            |
| -------- | ------- | ---------------------- |
| result   | boolean | Estado de la operación |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro de la operación   |
| message  | cadena  | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio: Editar esquema de vacunación

|                    |                                              |
| ------------------ | -------------------------------------------- |
| Descripción        | Actualiza un esquema de vacunación existente |
| Ruta               | /scheme                                      |
| Método             | PUT                                          |
| Formato de entrada | JSON                                         |

### Parámetros de entrada

| Atributo | Tipo   | Descripción           |
| -------- | ------ | --------------------- |
| id       | entero | id del esquema        |
| dosis    | entero | nuevo número de dosis |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción             |
| -------- | ------- | ----------------------- |
| result   | boolean | Estado de actualización |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro deL esquema       |
| message  | cadena  | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio: Crear vuelo

|                    |                        |
| ------------------ | ---------------------- |
| Descripción        | Crea un vuelo en la DB |
| Ruta               | /flight                |
| Método             | POST                   |
| Formato de entrada | JSON                   |

### Parámetros de entrada

| Atributo  | Tipo   | Descripción                     |
| --------- | ------ | ------------------------------- |
| origen    | entero | ciudad origen                   |
| destino   | entero | ciudad destino                  |
| pasajeros | entero | cantidad de pasajeros           |
| tipo      | entero | tipo de viaje (1 ida - 0 doble) |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción                  |
| -------- | ------- | ---------------------------- |
| result   | boolean | Estado de registro del vuelo |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro del vuelo         |
| message  | cadena  | Mensaje de resultado de la operación |

<hr style="border:2px solid gray"> </hr>

## Servicio: Hacer reseña

|                    |                                                      |
| ------------------ | ---------------------------------------------------- |
| Descripción        | Crea un nueva reseña de un usuario sobre un servicio |
| Ruta               | /review                                              |
| Método             | POST                                                 |
| Formato de entrada | JSON                                                 |

### Parámetros de entrada

| Atributo    | Tipo   | Descripción           |
| ----------- | ------ | --------------------- |
| id_servicio | entero | Id del servicio usado |
| comentario  | cadena | Reseña                |

### Parámetros de salida

|                             |      |
| --------------------------- | ---- |
| Formato de salida           | JSON |
| Código de respuesta exitosa | 200  |

### Parámetros de salida exitosa:

| Atributo | Tipo    | Descripción                     |
| -------- | ------- | ------------------------------- |
| result   | boolean | Estado de registro de la reseña |

### Parámetros de salida fallida:

|                             |     |
| --------------------------- | --- |
| Código de respuesta fallida | 400 |

### Parámetros de salida fallida:

| Atributo | Tipo    | Descripción                          |
| -------- | ------- | ------------------------------------ |
| status   | boolean | Estado de registro de la reseña      |
| message  | cadena  | Mensaje de resultado de la operación |

[Volver al índice](../indice.md "Título opcional del enlace")
