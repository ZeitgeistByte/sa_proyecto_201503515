# Mockups

## Web

<p>Al inicio se meustra la posibilidad de que usuario registrar</p>

![Registro](mockups/Seleccionregistro.png)

<p> Registro de turistas </p>

![Registro](mockups/Registroturista.png)

<p> Registro de empresas terceras</p>

![Registro](mockups/Registroterceros.png)

<p> Login </p>

![Registro](mockups/login.png)

<p> Recuperación de contraseña</p>

![Registro](mockups/recuperarContraseña.png)

<p> Crear espacio  </p>

![Registro](mockups/crearespacio.png)

<p> Crear espacio  </p>

![Registro](mockups/crearespaciocopy.png)

<p> Vista de espacios  </p>

![Registro](mockups/homeHotel.png)

<p> Vista de autos  </p>

![Registro](mockups/homeAutos.png)

<p> Crear vehículos </p>

![Registro](mockups/crearVehiculo.png)

<p> Carga masiva de vehículos </p>

![Registro](mockups/cargarVehiculo.png)

## Móvil

<p> Login móvil </p>

![Registro](mockups/loginmovil.png)

<p> Confirmación de login</p>

![Registro](mockups/confirmar.png)

<p> Filtrado de espacios </p>

![Registro](mockups/espacios.png)

<p> Filtrado de autos </p>

![Registro](mockups/autos.png)

<p> Resultado de búsqueda filtrada </p>

![Registro](mockups/autoscopy.png)

<p> Renta de espacios </p>

![Registro](mockups/espacios.png)

<p> Compra de vuelos</p>

![Registro](mockups/vuelos.png)

[Volver al índice](../indice.md "Título opcional del enlace")
