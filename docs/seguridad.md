# Seguridad de la aplicación

La seguridad de las aplicaciones web es fundamental para proteger los datos, los clientes y las organizaciones del robo de datos, por lo cual para esta aplicación se pretende lo siguiente: <br>

- Uso de JWT para la creación de tokens de acceso que permiten la propagación de identidad y privilegios.
- Validar todas las entradas ingresadas por los usuarios finales en el cliente, en cualquier formulario, para evitar cualquier tipo de inyección de código por parte de usuarios malintencionados.
- No almacenar contraseñas, ni datos sensibles de los usuarios en cookies o en el almacenamiento local del cliente.
- Usar un sistema rígido para la recuperación de contraseñas, se plantea el uso de autenticación en dos pasos, a través de un SMS o correo al usuario y que este ingrese un token.
- Almacenamiento cifrado de contraseñas.
- Cifrar datos sensibles durante la comunicación entre los clientes y los microservicios.
- Terminar sesiones luego de un período de inactividad.

[Volver al índice](../indice.md "Título opcional del enlace")
